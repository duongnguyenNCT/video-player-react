import React, { useState, useEffect } from 'react';

import './VideoQuantity.css'

const VideoQuantity = props => {
  const [isShowQuantities, setIsShowQuantities] = useState(false);

  const { pQuantities } = props;

  useEffect(() => {
    setIsShowQuantities(false);
  }, [props.flagToCloseQuantityBox])

  function onSetQuantity(_q) {
    if(props.setQuantity) {
      props.setQuantity(_q);
    }
    setIsShowQuantities(false);
  }

  const isActive = isShowQuantities ? ' is-active' : '';
  return (
    <div className="__video-resolution">
      <div className={'wrapper-resolution-selected' + isActive} onClick={() => setIsShowQuantities(!isShowQuantities)}>
        {props.pQuantity}p
      </div>
      <div className="resolution-list">
        <ul className="wrapper-resolution-list">
          { pQuantities ? pQuantities.map(re => 
            {
              const _c = props.pQuantity === re.quantity ? 'selected' : '';
              return <li key={re.quantity} className={'resolution-item ' + _c} onClick={() => onSetQuantity(re.quantity)}>
                <p className={re.isVip ? 'vip' : ''}>{re.quantity}p</p>
              </li>
          }):null }
        </ul>
      </div>
    </div>
  )
}

export default VideoQuantity;