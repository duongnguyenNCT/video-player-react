import React, { useState, useEffect, useRef } from 'react';

import './PullBar.css'

/**
 * The custom hook to listening event, that ensure to remove event when the hook is unmount
 * @param {String} eventName 
 * @param {Function} handler 
 * @param {Object} element 
 */
function useEventListener(eventName, handler, element = window) {
  const savedHandler = useRef();

  useEffect(() => {
    savedHandler.current = handler;
  }, [handler]);

  useEffect(
    () => {
      const isSupported = element && element.addEventListener;
      if (!isSupported) return;
      
      const eventListener = event => savedHandler.current(event);
      
      element.addEventListener(eventName, eventListener);
      
      return () => {
        element.removeEventListener(eventName, eventListener);
      };
    },
    [eventName, element]
  ); 

  // return event
}

/**
 * PullBar process 
 * @param {Object { 
 *  value: Number - the process of component,
 *  valueLoaded: Number - process loaded
 *  pWidth: Number - require when you, 
 *  getProcess: Function,
 *  getIsControlling: Function }} props 
 */
const  PullBar = props => {
  //- store percent process object
  const [ process, setProcess ] = useState(0);
  const [processLoaded, setProcessLoaded] = useState(0);

  const [isHovering, setIsHovering] = useState(false);

  const [ isPulling, setIsPulling ] = useState(false);

  const [ bulletInfo, setBulletInfo ] = useState({x: 0, y: 0, width: 0, height: 0});

  //- store the width pullbar when u cant get width.
  const [ widthPullBar, setWidthPullBar ] = useState(0);

  const refPullBar = useRef(null);
  // const refBullet = useRef(null);

  //- global param
  let g_BulletSize = 12;

  //- watch props.value
  useEffect(() => {
    //- process props value
    if(props.value !== null && props.value !== undefined && !Number.isNaN(Number(props.value))) {
      let width = null;
      if(refPullBar && refPullBar.current) width = refPullBar.current.getBoundingClientRect().width;
      onUpdateProcess(props.value ,width);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.value]); 

  useEffect(() => {
    if(props.valueLoaded !== null && props.valueLoaded !== undefined && !Number.isNaN(Number(props.valueLoaded))) {
      setProcessLoaded(props.valueLoaded)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.valueLoaded]); 

  useEffect(() => {
    setTimeout(() => {
      let _widthPullBar = null;
      if(refPullBar && refPullBar.current) _widthPullBar = refPullBar.current.getBoundingClientRect().width;

      if(props.pWidth !== null && props.pWidth !== undefined && !Number.isNaN(Number(props.pWidth))) {

        if(_widthPullBar === 0 && props.pWidth !== undefined) {
          _widthPullBar = Number(props.pWidth);
        }
      }
      if(props.value !== null && props.value !== undefined && !Number.isNaN(Number(props.value))) {
        onUpdateProcess(props.value ,_widthPullBar);
      } else {
        onUpdateProcess(process, _widthPullBar)
      }
    }, 0)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.flagUpdateProcessVideo]); 

  //- watch props.pWidth
  useEffect(() => {
    //- process props value
    if(props.pWidth !== null && props.pWidth !== undefined && !Number.isNaN(Number(props.pWidth))) {
      let _widthPullBar = null;
      if(refPullBar && refPullBar.current) _widthPullBar = refPullBar.current.getBoundingClientRect().width;

      if(_widthPullBar === 0 && props.pWidth !== undefined) {
        _widthPullBar = Number(props.pWidth);
      }

      onUpdateProcess(props.value ,_widthPullBar);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.pWidth]);

  useEffect(() => {
    function onMouseUp(e) {
      setIsPulling(false);

      props.getIsControlling(false);
      // onHandlePositionBullet(e);
      e.preventDefault();
    }

    if(isPulling) {
      window.addEventListener('mouseup', onMouseUp);
    }

    return () => {
      window.removeEventListener('mouseup', onMouseUp);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isPulling]); 

  //- listening event
  useEventListener('mousemove', onMouseMove);

  function getProcess(iProcess=null) {
    if(iProcess) {
      props.getProcess(iProcess)
    } else {
      props.getProcess(process);
    }
  }

  function getMousePosProcessWithBar(e, { left, top, width, height }) {
    let x = e.clientX - left - bulletInfo.width/2,
      y = e.clientY - (top + height / 2) - e.pageY;

    //- pull only x
    let dx = x - bulletInfo.x,
      xValue = bulletInfo.x + dx + bulletInfo.width/2;

    if(xValue < 0 ) {
      x = 0 - bulletInfo.width/2;
    }

    if(xValue > width) {
      x = width - bulletInfo.width/2;
    }

    return { x, y };
  }

  function onUpdateProcess(_process, _widthPullBar=null) {
    if(_widthPullBar && _widthPullBar !== 0 && _widthPullBar !== widthPullBar) {
      setWidthPullBar(_widthPullBar);
    }

    setProcess(Number(_process));

    let x = estimateBulletX(_widthPullBar ? _widthPullBar : widthPullBar, g_BulletSize, Number(_process));

    setBulletInfo({...bulletInfo, x, width: g_BulletSize})
  }

  function onMouseDown(e) {
    if(e.button === 0) {
      setIsPulling(true);
      props.getIsControlling(true);
      onHandlePositionBullet(e);
    }
  }
  function onMouseMove(e) {
    if(isPulling) {
      if(!isHovering && props.onMouseLeave) {
        setIsHovering(true);
        props.onMouseLeave(false);
      }
      const { process, posBullet, widthPullBar } = onHandlePositionBullet(e);

      if(props.getProcessHoverPullBar) {
        props.getProcessHoverPullBar(process, { x: posBullet.x, widthPullBar });
      }
      e.stopPropagation();
    }

    e.preventDefault();
  }
  function onHoverBarBg(e) {
    if(props.onMouseLeave && !isHovering && !isPulling) {
      setIsHovering(true);
      props.onMouseLeave(false);
    }
  }

  function onMoveBarBg(e) {
    if(isHovering && !isPulling) {
      onHandleProcessHoverBarBg(e);
    }
  }

  function onMouseLeaveBarBg() {
    if(props.onMouseLeave && !isPulling) {
      setIsHovering(false);

      props.onMouseLeave(true);
    }
  }

  function onHandleProcessHoverBarBg(e) {
    const { left, top, width, height } = refPullBar.current.getBoundingClientRect();
    const { x, y } = getMousePosProcessWithBar(e, { left, top, width, height });

    const process = estimateProcess(width, x);
    props.getProcessHoverPullBar(process, {x, y, widthPullBar: width});
  }

  function onHandlePositionBullet(e) {
    const { left, top, width, height } = refPullBar.current.getBoundingClientRect();

    const { x, y } = getMousePosProcessWithBar(e, { left, top, width, height });

    setBulletInfo({...bulletInfo, x, y});

    const process = estimateProcess(width, x + bulletInfo.width/2);

    setProcess(process);
    getProcess(process);

    return { process, posBullet: {x,y}, widthPullBar: width }
  }

  function estimateProcess(maxVal, val) {
    return val / maxVal * 100;
  }

  function estimateBulletX(maxVal, widthObject, process) {
    return (maxVal * process / 100) - (widthObject / 2);
  }

  const _pulling = isPulling ? 'is-pulling' : '';
  return (
    <div className={'__pull-bar ' + _pulling }>
      <div className="wrapper" 
        ref={refPullBar} 
        onMouseDown={onMouseDown}
        onMouseOver={onHoverBarBg} 
        onMouseMove={onMoveBarBg} 
        onMouseLeave={onMouseLeaveBarBg}
      >
        <div 
          className="bar-bg"
        >
          <div className="bar-bg-loaded" style={{ width: processLoaded + '%' }}></div>
        </div>
        <div className="bar-current" onMouseDown={onMouseDown} style={{"width": process + '%'}}></div>
        <div 
          className="bullet-control" 
          style={{
            left: bulletInfo.x + 'px',
            width: `${g_BulletSize}px`,
            height: `${g_BulletSize}px`
            }}
          ></div>
      </div>
    </div>
  )
}

export default PullBar;