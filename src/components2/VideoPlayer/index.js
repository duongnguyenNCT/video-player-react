import React from 'react';
import PropTypes from 'prop-types';

import ReactPlayer from 'react-player';
import PullBar from '../PullBar';
import VideoQuantity from '../VideoQuantity';

import './VideoPlayer.css';
/**
 * 
 * @param {Number} second Number second you want to convert with format h:mm:ss 
 */
function parseNumberToTime(second=null) {
  let h, m, s;
  if(second != null 
    && Number.isInteger(Number(second))) {
    let _second = Number(second);
    if(_second < 60) return `00:${(second >= 0 && second < 10 ? '0' : '') + second}`;
    h = parseInt(_second / 60 / 60);
    if (h >= 1) {
      m = parseInt((_second - h * 60 * 60) / 60);
      s = (_second - h * 60 * 60) % 60;
    } else {
      m = parseInt(_second / 60);
      s = (_second - h * 60 * 60) % 60;
    }

    return `${h > 0 ? h + ':' : ''}${(m >= 0 && m < 10 ? '0' : '')+ m}:${(s >= 0 && s < 10 ? '0' : '') + s}`;
  }
}

/**
 * The Component Video Player 
 * @param {Number} pInitTimer the initial second video, if undefined, the value is 0
 * @param {Object {
 *  url: String - path, url video, youtube, ...
 *  thubnail: String - url image thumbnail
 * }} pData required
 * @param {Boolean} pIsTurnOnPIP turn on/of feature PiP player. default: false
 * @param {Boolean} autoplay default false
 * @param {String} pQuantity the props manage quantity video
 * @param {Array} pQuantities the props manage array quatity is variable for the video
 * @param {Function} getQuantity get quantity user click 
 */
class VideoPlayer extends React.Component {
  constructor(props) {
    super(props);

    this.detectBrowser = null;
    this.timerPlayVideo = null;
    this.timerProcess = null;
    this.timerPreviewProcess = null;
    this.timerMouseHidden = null;

    this.mouseHidden = false;

    this.videoPreviewSize = { width: 160, height: 90 };
    this.videoSecondLoaded = 0;

    this.state = {
      isPlaying: true,
      isShowThumbnail: true,

      videoDuration: null,
      videoTimer: null,
      videoVolume: 0.5,
      videoPip: false,
      videoSecondLoaded: 0,
      isVideoLoading: false,
      videoErrorMessage: null,
      flagUpdateProcessVideo: false,
      flagToCloseQuantityBox: false,
      
      videoPreviewPosX: 0,
      isShowReview: false,
      videoPreviewTimer: 0,
      isPreviewLoading: false,

      isControllingProcess: false,
      isHiddenControlBar: false,
      isControllingVolume: false,
      isFullscreen: false,
      isMuteVideo: false,
      isReadyPlay: false
    }

    this.refMyVideo = React.createRef();
    this.refVideoPlayer = React.createRef();
    this.refVideoPreview = React.createRef();
    this.refLisenterKeyPress = React.createRef();

    this.getVideoDuration = this.getVideoDuration.bind(this);
    this.getSecondsLoaded = this.getSecondsLoaded.bind(this);
    this.getProcessVideoPreview = this.getProcessVideoPreview.bind(this);
    this.setProcessVideo = this.setProcessVideo.bind(this);
    this.setVideoVolume = this.setVideoVolume.bind(this);
    this.setVideoQuantity = this.setVideoQuantity.bind(this);

    this.handlePlayPauseVideo = this.handlePlayPauseVideo.bind(this);
    this.handleThumbnail = this.handleThumbnail.bind(this);
    this.handleOnOffMuteVideo = this.handleOnOffMuteVideo.bind(this);
    this.handleKeyPressVideo = this.handleKeyPressVideo.bind(this);
    this.handleMouseupVideo = this.handleMouseupVideo.bind(this);
    this.handleMouseoutVideo = this.handleMouseoutVideo.bind(this);
    this.handleMousemoveVideo = this.handleMousemoveVideo.bind(this);
    this.handleMouseHidden = this.handleMouseHidden.bind(this);
    this.handleVideoError = this.handleVideoError.bind(this);
    this.handleVideoPip = this.handleVideoPip.bind(this);
    this.turnOnOffProcessControl = this.turnOnOffProcessControl.bind(this);

    this.onReadyVideo = this.onReadyVideo.bind(this);
    this.onPlayVideo = this.onPlayVideo.bind(this);
    this.onPauseVideo = this.onPauseVideo.bind(this);
    this.onProgress = this.onProgress.bind(this);
    this.onClickVideo = this.onClickVideo.bind(this);
    this.onFullscreen = this.onFullscreen.bind(this);
    this.onMouseLeaveProcessVideo = this.onMouseLeaveProcessVideo.bind(this);
  }

  componentDidMount() {
    //- detect browser
    const { pIsTurnOnPIP } = this.props;
    var isFirefox = typeof InstallTrigger !== 'undefined';
    if(isFirefox) {
      this.detectBrowser = 'firefox'
    }

    this.detectRef('refVideoPlayer', () => {
      this.refVideoPlayer.current.onfullscreenchange = () => {
        const { flagUpdateProcessVideo, flagToCloseQuantityBox } = this.state;
        this.setState({
          isFullscreen: document.fullscreenElement,
          flagUpdateProcessVideo: !flagUpdateProcessVideo,
          flagToCloseQuantityBox: !flagToCloseQuantityBox
        });
      }

      this.refVideoPlayer.current.oncontextmenu = (e) => {
        e.preventDefault();
      }
      this.refVideoPlayer.current.addEventListener('mouseup', this.handleMouseupVideo);
      this.refVideoPlayer.current.addEventListener('mousemove', this.handleMousemoveVideo);
    });

    this.detectRef('refLisenterKeyPress', () => {
      this.refLisenterKeyPress.current.addEventListener('keypress', this.handleKeyPressVideo);
    });

    document.addEventListener('mousedown', this.handleMouseoutVideo);


    //- Firefox dont support navigator.mediaSession
    if(pIsTurnOnPIP) {
      if(this.detectBrowser !== 'firefox') {
        navigator.mediaSession.setActionHandler('previoustrack', function() {
          console.log('pre pip')
        });
        navigator.mediaSession.setActionHandler('nexttrack', function() {
          console.log('next pip')
        });
      } else {
        console.warn("Firefox don't support next, pre button pip player.");
      }
    }
  }

  componentWillUnmount() {
    if(this.refVideoPlayer && this.refVideoPlayer.current) {
      this.refVideoPlayer.current.removeEventListener('mouseup', this.handleMouseupVideo);
      this.refVideoPlayer.current.removeEventListener('mousemove', this.handleMousemoveVideo);
    }

    document.removeEventListener('mousedown', this.handleMouseoutVideo);
  }

  detectRef(refName=null, cb=() => {}) {
    if(refName) {
      if(this[refName] && this[refName].current) {
        cb();
      }
    }
  }

  getVideoDuration(duration) {
    const videoTimer = duration - this.props.pInitTimer;
    this.setState({
      videoDuration: duration,
      videoTimer
    });

    this.refMyVideo.current.seekTo(this.props.pInitTimer);
  }

  getSecondsLoaded(secondLoaded = null) {
    let s;
    if(secondLoaded === null) s = this.refMyVideo.current.getSecondsLoaded();
    s = secondLoaded;
    if(s !== null) {
      const { videoSecondLoaded } = this.state;
      if(videoSecondLoaded !== s) {
        this.videoSecondLoaded = s;
      }
    }
  }

  getProcessVideoPreview(process, {x, widthPullBar}) {
    if(Number.isNaN(process)) return;

    const { videoDuration } = this.state;
    let s = (videoDuration * process) / 100;

    clearTimeout(this.timerPreviewProcess);
    this.timerPreviewProcess = setTimeout(() => {
      this.setState({
        isPreviewLoading: false
      })
    }, 400);

    if(this.refVideoPreview.current) {
      this.refVideoPreview.current.seekTo(s);
    }

    let _x = x - this.videoPreviewSize.width/2;
    if(_x <= 0) {
      _x = 0;
    } else if((_x + this.videoPreviewSize.width) > widthPullBar) {
      _x = widthPullBar - 160
    }

    this.setState({
      videoPreviewPosX: _x,
      videoPreviewTimer: s,
      isPreviewLoading: true
    })
  }

  onReadyVideo() {
    const { isShowThumbnail, isPlaying, isReadyPlay } = this.state;
    const { autoplay } = this.props;

    if((isShowThumbnail && !autoplay && isPlaying && !isReadyPlay) ) { //- || this.detectBrowser === 'firefox'
      this.setState({
        isPlaying: false,
        isReadyPlay: true
      })
    } else if(isPlaying && !isReadyPlay){
      this.setState({
        isReadyPlay: true,
        isShowThumbnail: false
      })
    }
  }

  onClickVideo(e) {
    // hanlde play/pause
    const { isPlaying } = this.state;
    this.handlePlayPauseVideo(!isPlaying);

    e.preventDefault();
  }

  setProcessVideo(process) {
    if(Number.isNaN(process)) return;
    
    const { videoDuration } = this.state;
    let s = (videoDuration * process) / 100;

    this.setState({
      videoTimer: videoDuration - s
    });

    clearTimeout(this.timerProcess);

    this.timerProcess = setTimeout(() => {
      this.setState({
        isVideoLoading: false
      });
    }, 400);

    this.refMyVideo.current.seekTo(s);
    this.setState({
      isVideoLoading: true
    });
  }

  setVideoVolume(percentVolume) {
    if(Number.isNaN(percentVolume)) return;
    let temp = percentVolume/100;

    this.setState({
      videoVolume: temp
    })
  }

  setVideoQuantity(quantity) {
    if(this.props.getQuantity) {
      this.props.getQuantity(quantity);
    }
  }

  handlePlayPauseVideo(iIsPlaying=true) {
    const { isShowThumbnail } = this.state;
    this.setState({
      isPlaying: iIsPlaying
    });

    if(isShowThumbnail) {
      this.handleThumbnail();
    }
  }

  handleThumbnail() {
    const { isShowThumbnail } = this.state;
    if(isShowThumbnail) {
      this.setState({
        isShowThumbnail: false
      })
    }
  }

  handleOnOffMuteVideo() {
    const { isMuteVideo } = this.state;

    this.setState({
      isMuteVideo: !isMuteVideo
    })
  }

  handleKeyPressVideo(evt) {
    if(evt.keyCode === 32) {
      const { isPlaying } = this.state;
      this.handlePlayPauseVideo(!isPlaying);
    }

    evt.preventDefault();
  }

  handleMouseupVideo() {
    this.refLisenterKeyPress.current.focus();
  }

  handleMouseoutVideo() {
    this.refLisenterKeyPress.current.focus();
  }

  handleMousemoveVideo() {
    const { isHiddenControlBar } = this.state;
    if(!this.mouseHidden) {
      if(isHiddenControlBar) {
        this.setState({
          isHiddenControlBar: false
        });
      }
      clearTimeout(this.timerMouseHidden);

      this.refVideoPlayer.current.style.cursor = 'pointer';
      this.timerMouseHidden = setTimeout(this.handleMouseHidden, 1000);
    }
  }

  handleMouseHidden() {
    const { isFullscreen, isHiddenControlBar } = this.state;
    if(!isFullscreen) return;
    this.refVideoPlayer.current.style.cursor = 'none';
    if(!isHiddenControlBar) {
      this.setState({
        isHiddenControlBar: true
      });
    }
    this.mouseHidden = true;
    setTimeout(() => {
      this.mouseHidden = false;
    }, 500);
  }

  handleVideoPip() {
    const { videoPip } = this.state;
    this.setState({ videoPip: !videoPip });
  }

  handleVideoError(err) {
    if(err) {
      // const _ = "Error is during processing. Refresh page please";
      console.error(err.name);
      if(err.name === 'NotAllowedError') {
        this.setState({
          isPlaying: false
        });

        return;
      }
      
      this.setState({
        videoErrorMessage: err
      })
    }
  }

  onFullscreen() {
    if(document.fullscreenElement) {
      document.exitFullscreen();
    } else {
      this.refVideoPlayer.current.requestFullscreen();
    }
  }

  onProgress({played, playedSeconds, loaded, loadedSeconds}) {
    const { videoDuration } = this.state;

    this.setState({
      videoTimer: videoDuration - playedSeconds
    });

    this.getSecondsLoaded(loadedSeconds);
  }

  onPlayVideo() {
    const { isPlaying, isReadyPlay } = this.state;
    if(!isPlaying && isReadyPlay) {
      this.setState({
        isPlaying: true
      })
    }
  }

  onPauseVideo() {
    const { isPlaying, isControllingProcess } = this.state;

    if(!isControllingProcess && isPlaying) {
      this.setState({
        isPlaying: false
      })
    }
  }

  onMouseLeaveProcessVideo(isLeave) {
    const { isShowReview } = this.state;
    if(isShowReview === !isLeave) return;
    this.setState({
      isShowReview: !isLeave
    })
  }

  turnOnOffProcessControl(stateName='isControllingProcess', isOn=false) {
    if(this.state[stateName] !== isOn) {
      this.setState({
        [stateName]: isOn
      });
    }
  }

  renderProcessBar() {
    const { videoDuration, videoPreviewPosX, videoPreviewTimer, videoTimer,
      isShowReview, isPreviewLoading,
      flagUpdateProcessVideo } = this.state,
      {pData} = this.props;

    const processVideo =((videoDuration - videoTimer) / (videoDuration !==0 ? videoDuration : 1)) * 100,
      processLoadedVideo = (this.videoSecondLoaded / (videoDuration !==0 ? videoDuration : 1)) *  100;

    return (
      <div className="process-bar">
        <div 
          className="video-preview" 
          style={{
            left: videoPreviewPosX + 'px', 
            top: `-${this.videoPreviewSize.height + 7}px`,
            display: isShowReview ? 'flex' : 'none',
            width: `${this.videoPreviewSize.width}px`,
            height: `${this.videoPreviewSize.height}px`
          }} 
          onClick={this.onPlayVideo}
        >
          <ReactPlayer
            className="_wrapper-video-preview"
            ref={this.refVideoPreview}
            url={pData.src}
            width={isPreviewLoading ? '0' : '100%'}
            height={isPreviewLoading ? '0' : '100%'}
            muted
            style={{
              visibility: isPreviewLoading ? 'hidden' : 'visible'
            }}
          />
          <div className="container-loader" style={{
            visibility: !isPreviewLoading ? 'hidden' : 'visible'
          }} >
            <div className="loader"></div>
          </div>
          <p><span>{parseNumberToTime(parseInt(videoPreviewTimer))}</span></p>
        </div>
        <PullBar
          value={processVideo}           
          valueLoaded={processLoadedVideo}
          getProcess={this.setProcessVideo} 
          getIsControlling={(isOn) => this.turnOnOffProcessControl('isControllingProcess', isOn)}
          getProcessHoverPullBar={this.getProcessVideoPreview}
          onMouseLeave={this.onMouseLeaveProcessVideo}
          flagUpdateProcessVideo={flagUpdateProcessVideo}
        />
      </div>
    )
  }

  renderVolumeControl() {
    const { videoVolume, 
      isMuteVideo, isControllingVolume } = this.state;

    const percentVolume = (videoVolume * 100),
      _controllingVolume = isControllingVolume ? 'is-controlling' : '';

    let _levelVolume = '';
    if(isMuteVideo || videoVolume === 0) _levelVolume = 'muted';
      else if(videoVolume >= 0.5) _levelVolume = 'max';
      else if(videoVolume >= 0.05) _levelVolume = 'min';
    
    return (
      <div className={'volume-control ' + _controllingVolume}>
        <div className={'volume-icon ic_volume_on_min ' + _levelVolume} onClick={this.handleOnOffMuteVideo}></div>
        <div className="volume-bar">
          <div className="wrapper-pullbar">
            <PullBar 
              value={percentVolume} 
              pWidth={70} 
              valueLoaded={100}
              getProcess={this.setVideoVolume} 
              getIsControlling={(isOn) => this.turnOnOffProcessControl('isControllingVolume', isOn)}
            />
          </div>
        </div>
      </div>
    )
  }

  renderControlsRight() {
    const { pQuantities, pQuantity, pIsTurnOnPIP } = this.props;
    const { flagToCloseQuantityBox } = this.state;

    return (
      <div className="controls right">
        { (this.detectBrowser !== 'firefox' && pIsTurnOnPIP) ? 
          <div className="control-item ic">
            <div className="pip-control" onClick={this.handleVideoPip}></div>
          </div> : null }
        <div className="control-item">
          <VideoQuantity pQuantities={pQuantities} pQuantity={pQuantity} setQuantity={this.setVideoQuantity} flagToCloseQuantityBox={flagToCloseQuantityBox} />
        </div>
        <div className="control-item ic">
          <div className="fullscreen-control ic_full_screen" onClick={this.onFullscreen}></div>
        </div>
      </div>
    )
  }

  renderControlsLeft() {
    const { videoTimer, videoDuration } = this.state;
    return (
      <div className="controls left">
        {/* play control */}
        <div className="control-item ic">
          <div className="play-control ic_play_normal" onClick={this.onClickVideo}></div>
        </div>
        {/* next control */}
        <div className="control-item ic">
          <div className="next-control ic_next_normal"></div>
        </div>
        {/* volume control */}
        <div className="control-item">
         { this.renderVolumeControl() }
        </div>
        {/* second info */}
        <div className="control-item ic">
          <label>{parseNumberToTime(parseInt(videoTimer))} &nbsp;/ <span>{parseNumberToTime(parseInt(videoDuration))}</span></label>
        </div>
      </div>
    )
  }

  render() {
    const { pData } = this.props,
      { isPlaying, isShowThumbnail, isControllingProcess, isControllingVolume, 
        isFullscreen, isMuteVideo, isReadyPlay, isVideoLoading,
        videoVolume, videoErrorMessage, videoPip,
        isHiddenControlBar } = this.state;

    if(!pData) return null;

    const _controllingProcess = isControllingProcess || isControllingVolume ? 'is-controlling': '',
      _fullscreen = isFullscreen ? 'is-fullscreen' : '',
      _isPlaying = (isPlaying && !isControllingProcess && isReadyPlay),
      _hiddenControl = isHiddenControlBar ? ' hidden' : '',
      _isVideoLoading = (isVideoLoading || videoErrorMessage !== null || !isReadyPlay);  

    return (
      <div className={'__video-player ' + _fullscreen} ref={this.refVideoPlayer}>
        <div className="wrapper">
          {/* video thumbnail, mask */}
          <div 
            className={'mask-play-container' + (isShowThumbnail ? ' thumbnail' : '') + (!isPlaying ? ' show' : ' hide')} 
            style={{ backgroundImage: isShowThumbnail ? `url(${pData.poster})` : 'none' } }
            onClick={this.onClickVideo}
          >
            <div className="ic_play_normal"></div>
          </div>

          {/* video source */}
          <div 
            className={`video ${_isPlaying ? 'playing' : '' }`}
            onClick={this.onClickVideo}
          >
            {
              pData ? 
              <>
                <ReactPlayer
                  className="__wrapper-video"
                  ref={this.refMyVideo}
                  url={pData.src}
                  playing={_isPlaying}
                  light={false}
                  pip={videoPip}
                  onReady={this.onReadyVideo}
                  onPlay={this.onPlayVideo}
                  onPause={this.onPauseVideo}
                  onError={this.handleVideoError}
                  onDuration={(duration) => this.getVideoDuration(duration)}
                  onProgress={({played, playedSeconds, loaded, loadedSeconds}) => this.onProgress({played, playedSeconds, loaded, loadedSeconds})}
                  volume={videoVolume}
                  width={ _isVideoLoading ? '0' : '100%' }
                  height={ _isVideoLoading ? '0' : '100%' }
                  muted={isMuteVideo}
                />
                <div className="container-loader" style={{
                  visibility: (!_isVideoLoading) ? 'hidden' : 'visible',
                  width: (!_isVideoLoading) ? '0' : '100%',
                  height: (!_isVideoLoading) ? '0' : '100%'
                }} >
                  <div className="big loader"></div>
                </div>
              </>
               : null
            }
          </div>

          {/* proccess bar */}
          <div className={'proccess-control-bar ' + _controllingProcess + _hiddenControl} style={{ display: isReadyPlay ? '' : 'none' }}>
            { this.renderProcessBar() }
            <div className="control-group">
              { this.renderControlsLeft() }

              { this.renderControlsRight() }

            </div>
          </div>

        </div>

        <div id="listeningKeyPress" ref={this.refLisenterKeyPress} contentEditable></div>
      </div>
    )
  }
}

VideoPlayer.propTypes = {
  pInitTimer: PropTypes.number,
  pData: PropTypes.object,
  autoPlay: PropTypes.bool,
  pIsTurnOnPIP: PropTypes.bool
}

VideoPlayer.defaultProps = {
  pInitTimer: 0,
  pData: null,
  autoPlay: false,
  pIsTurnOnPIP: false
}

export default VideoPlayer;