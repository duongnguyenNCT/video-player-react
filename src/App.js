import React, { useState } from 'react';

import VideoPlayer from './components2/VideoPlayer';

import './App.css';

const g_video = {
  id: 0,
  name: 'Chan Ai',
  src: 'https://vredir.nixcdn.com/PreNCT18/SauNay-TangPhuc-6236744.mp4?st=hANPgSDiB0qKStkYT33SGg&e=1586226052&t=1586139652273',
  poster: 'https://avatar-nct.nixcdn.com/mv/2020/02/17/e/d/b/1/1581940951911_640.jpg',
  width_original: 600,
  singers: ['Single 1', 'Single 2'],
  authors: ['Author 1']
}

const g_quantities = [
  { quantity: '1080', isVip: true },
  { quantity: '720', isVip: true },
  { quantity: '480' },
  { quantity: '360' },
]

function App() {
  let _width = 16 * 50,
    _height = _width * 9 /16;

  const [ videoQuantity, setVideoQuantity ] = useState('360');

  function getQuantity(q) {
    setVideoQuantity(q);
  }
  return (
    <div className="App">
      <div className="wrapper-video-player" style={{width: _width + 'px', height: _height + 'px' }}>
        <VideoPlayer 
          pData= {g_video} 
          autoplay={true} 
          // pInitTimer={100}
          pIsTurnOnPIP={true}
          pQuantities={g_quantities}
          pQuantity={videoQuantity}
          getQuantity={getQuantity}
        />
      </div>
    </div>
  );
}

export default App;
