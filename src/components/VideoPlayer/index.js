import React, { Component } from 'react';

import './VideoPlayer.css';

/**
 * 
 * @param {Number} second Number second you want to convert with format h:mm:ss 
 */
function parseNumberToTime(second=null) {
  let h, m, s;
  if(second != null 
    && Number.isInteger(Number(second))) {
    let _second = Number(second);
    if(_second < 60) return `00:${(second >= 0 && second < 10 ? '0' : '') + second}`;
    h = parseInt(_second / 60 / 60);
    if (h >= 1) {
      m = parseInt((_second - h * 60 * 60) / 60);
      s = (_second - h * 60 * 60) % 60;
    } else {
      m = parseInt(_second / 60);
      s = (_second - h * 60 * 60) % 60;
    }

    return `${h > 0 ? h + ':' : ''}${(m >= 0 && m < 10 ? '0' : '')+ m}:${(s >= 0 && s < 10 ? '0' : '') + s}`;
  }
}

class VideoPlayer extends Component {
  constructor(props) {
    super();

    this.state = {
      isPause: true,
      isShowThumbnail: true,
      videoThumbnail: '',
      videoTimer: 0
    }

    this.onClickVideo = this.onClickVideo.bind(this);
    this.onHandleVideo = this.onHandleVideo.bind(this);
  }

  componentDidMount() {
    this.setState({
      videoThumbnail: require(`../../${this.props.pData.thumbnail}`),
      videoTimer: this.props.pData.duration
    });
  }

  timerPlaying = null;

  onClickVideo() {
    this.onHandleVideo();
  }

  onHandleVideo() {
    const { isPause } = this.state,
      elmVideo = document.getElementById('_video-src');

    if(elmVideo) {
      if(isPause) {
        elmVideo.play();
        this.timerPlaying = setInterval(() => {
          if(this.state.videoTimer <= 0) {
            clearInterval(this.timerPlaying);
            return;
          }
          this.setState((prev) => ({
            videoTimer: parseInt(elmVideo.duration) - parseInt(elmVideo.currentTime)
          }))
        }, 1000)
      } else {
        clearInterval(this.timerPlaying);
        elmVideo.pause();
      }

      this.setState({
        isPause: !isPause,
        isShowThumbnail: false
      })
    }
  }

  render() {

    const { pData } = this.props;
    const { isShowThumbnail, videoThumbnail, isPause, videoTimer } = this.state;
    return(
      <div id="__video-player" className="__video-player">
        {/* play/pause mask */}
        <div className="wrapper">
          {/* video thumbnail, mask */}
            <div 
              className={'mask-play-container' + (isShowThumbnail ? ' thumbnail' : '') + (isPause ? ' show' : ' hide')} 
              style={{ backgroundImage: isShowThumbnail ? `url(${videoThumbnail})` : 'none' } }
            >
              <div></div>
              {/* <img src={require(`../../assets/icons/ic_play.png`)} alt="" /> */}
            </div>
          {/* video source */}
          <div className={`video ${isPause ? '' : 'playing'}`}>
            { pData ? 
              <video id="_video-src" width={pData.width_original} onClick={this.onClickVideo}>
                <source src={require(`../../${pData.src}`)} ></source>
                Video not supported
              </video> : null
            }
          </div>

          {/* proccess bar */}
          <div className="proccess-control-bar">
            <div className="proccess-bar"></div>
            <div className="control-group">
            <div className="controls-left">
              {/* play control */}
              <div className="control-item">
                <div className="play-control" onClick={this.onHandleVideo}></div>
              </div>
              {/* next control */}
              <div className="control-item">
                <div className="next-control"></div>
              </div>
              {/* volume control */}
              <div className="control-item">
                <div className="volume-control"></div>
                <div className="volume-bar"></div>
              </div>
              {/* second info */}
              <div className="control-item">
                <label>{parseNumberToTime(videoTimer)} | {parseNumberToTime(pData.duration)}</label>
              </div>
            </div>
              <div className="controls-right">
                <div className="control-item">
                  <div className="fullscreen-control"></div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    )
  }
}

export default VideoPlayer;