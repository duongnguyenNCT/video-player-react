/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'nct_font\'">' + entity + '</span>' + html;
	}
	var icons = {
		'ic_music_note': '&#xe97b;',
		'ic_active_qrcode': '&#xe97a;',
		'ic_bye_mrtui': '&#xe979;',
		'ic_close_round': '&#xe93c;',
		'ic_library_normal': '&#xe93f;',
		'ic_lyric_card': '&#xe941;',
		'ic_playlist_cloud': '&#xe943;',
		'ic_timmer_normal': '&#xe944;',
		'ic_video_cloud': '&#xe95e;',
		'ic_vip_circle_normal': '&#xe95f;',
		'ic_gift_vip': '&#xe974;',
		'ic_history': '&#xe975;',
		'ic_notification_active': '&#xe976;',
		'ic_notification_normal': '&#xe977;',
		'ic_upload': '&#xe978;',
		'ic_new': '&#xe969;',
		'ic_equal': '&#xe96a;',
		'ic_back2_normal': '&#xe96b;',
		'ic_next2_normal': '&#xe96c;',
		'ic_license': '&#xe96d;',
		'ic_headphone': '&#xe96e;',
		'ic_add_song': '&#xe96f;',
		'ic_volume_off': '&#xe970;',
		'ic_volume_on': '&#xe971;',
		'ic_artist2_active': '&#xe972;',
		'ic_artist2_normal': '&#xe973;',
		'ic_quality_128_line': '&#xe963;',
		'ic_quality_320_line': '&#xe964;',
		'ic_quality_360_line': '&#xe965;',
		'ic_quality_480_line': '&#xe966;',
		'ic_quality_720_line': '&#xe967;',
		'ic_quality_1080_line': '&#xe968;',
		'ic_replay_normal': '&#xe962;',
		'ic_camera_normal': '&#xe961;',
		'ic_expand_down': '&#xe960;',
		'ic_vip_qrcode': '&#xe95b;',
		'ic_vip_fulltext': '&#xe95c;',
		'ic_vip_text': '&#xe95d;',
		'ic_normal_screen': '&#xe959;',
		'ic_full_screen': '&#xe95a;',
		'ic_share_small': '&#xe956;',
		'ic_heart_plus_small': '&#xe957;',
		'ic_download_small': '&#xe958;',
		'ic_warning_small': '&#xe954;',
		'ic_checked_small': '&#xe955;',
		'ic_minus_small': '&#xe952;',
		'ic_plus_small': '&#xe953;',
		'ic_accordion': '&#xe951;',
		'ic_fillter_small': '&#xe950;',
		'ic_loading': '&#xe94f;',
		'ic_close_small': '&#xe94e;',
		'ic_wap_return': '&#xe947;',
		'ic_add_normal': '&#xe948;',
		'ic_wap_next': '&#xe946;',
		'ic_backtop_normal': '&#xe945;',
		'ic_wap_back': '&#xe93e;',
		'ic_wap_search': '&#xe940;',
		'ic_wap_menu': '&#xe942;',
		'ic_3g_normal': '&#xe936;',
		'ic_vip_normal': '&#xe937;',
		'ic_keyboard_delete': '&#xe92d;',
		'ic_keyboard_space': '&#xe92e;',
		'ic_list_lyric_normal': '&#xe92f;',
		'ic_list_playing_normal': '&#xe930;',
		'ic_more_normal': '&#xe931;',
		'ic_nosignal_normal': '&#xe932;',
		'ic_clean_normal': '&#xe912;',
		'ic_infomation_normal': '&#xe918;',
		'ic_language_normal': '&#xe919;',
		'ic_logout_normal': '&#xe938;',
		'ic_login_normal': '&#xe91a;',
		'ic_uncheck_normal': '&#xe91b;',
		'ic_checked_normal': '&#xe949;',
		'ic_quality_128': '&#xe91c;',
		'ic_quality_320': '&#xe91d;',
		'ic_quality_360': '&#xe91e;',
		'ic_quality_480': '&#xe91f;',
		'ic_quality_720': '&#xe920;',
		'ic_quality_1080': '&#xe921;',
		'ic_arrow_down': '&#xe922;',
		'ic_arrow_up': '&#xe923;',
		'ic_shuffle_normal': '&#xe924;',
		'ic_continue_normal': '&#xe933;',
		'ic_repeat_normal': '&#xe934;',
		'ic_repeat_one_normal': '&#xe935;',
		'ic_play_normal': '&#xe925;',
		'ic_pause_normal': '&#xe926;',
		'ic_back_normal': '&#xe927;',
		'ic_next_normal': '&#xe928;',
		'ic_backward_normal': '&#xe929;',
		'ic_forward_normal': '&#xe92a;',
		'ic_heart_active': '&#xe92b;',
		'ic_heart_normal': '&#xe92c;',
		'ic_move_down': '&#xe914;',
		'ic_move_left': '&#xe915;',
		'ic_move_right': '&#xe916;',
		'ic_move_up': '&#xe917;',
		'ic_return_normal': '&#xe911;',
		'ic_play_circle': '&#xe913;',
		'ic_news_active': '&#xe94a;',
		'ic_news_normal': '&#xe94b;',
		'ic_artist_active': '&#xe94c;',
		'ic_artist_normal': '&#xe94d;',
		'ic_top100_active': '&#xe939;',
		'ic_top100_normal': '&#xe93a;',
		'ic_song_active': '&#xe93b;',
		'ic_song_normal': '&#xe93d;',
		'ic_topic_active': '&#xe90f;',
		'ic_topic_normal': '&#xe910;',
		'ic_setting_active': '&#xe901;',
		'ic_setting_normal': '&#xe902;',
		'ic_chart_active': '&#xe903;',
		'ic_chart_normal': '&#xe904;',
		'ic_playlist_active': '&#xe905;',
		'ic_playlist_normal': '&#xe906;',
		'ic_video_active': '&#xe907;',
		'ic_video_normal': '&#xe908;',
		'ic_mymusic_active': '&#xe909;',
		'ic_mymusic_normal': '&#xe90a;',
		'ic_search_active': '&#xe90b;',
		'ic_search_normal': '&#xe90c;',
		'ic_home_active': '&#xe90d;',
		'ic_home_normal': '&#xe90e;',
		'ic_logo': '&#xe900;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/ic_[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
